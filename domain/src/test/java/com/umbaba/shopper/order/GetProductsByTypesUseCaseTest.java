package com.umbaba.shopper.order;

import com.umbaba.entity.ProductEntity;
import com.umbaba.entity.ProductTypeEntity;
import com.umbaba.shopper.ProductTestData;
import com.umbaba.shopper.boundries.IProductsGateway;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static com.umbaba.shopper.ProductTestData.PRODUCT_NAME_0;
import static com.umbaba.shopper.ProductTestData.PRODUCT_NAME_1;
import static com.umbaba.shopper.ProductTestData.PRODUCT_NAME_2;
import static com.umbaba.shopper.ProductTestData.PRODUCT_PRICE_0;
import static com.umbaba.shopper.ProductTestData.PRODUCT_PRICE_1;
import static com.umbaba.shopper.ProductTestData.PRODUCT_PRICE_2;
import static com.umbaba.shopper.ProductTestData.PRODUCT_TYPE_UUID_0;
import static com.umbaba.shopper.ProductTestData.PRODUCT_TYPE_UUID_1;
import static com.umbaba.shopper.ProductTestData.PRODUCT_TYPE_UUID_2;
import static com.umbaba.shopper.ProductTestData.PRODUCT_UUID_0;
import static com.umbaba.shopper.ProductTestData.PRODUCT_UUID_1;
import static com.umbaba.shopper.ProductTestData.PRODUCT_UUID_2;


public class GetProductsByTypesUseCaseTest {
    @Mock
    IProductsGateway productsGateway;
    private GetProductsByTypesUseCase getProductsByTypesUseCase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        getProductsByTypesUseCase = new GetProductsByTypesUseCase(productsGateway);
        Mockito.doReturn(Observable.just(ProductTestData.PRODUCT_ENTITIES)).when(productsGateway).products();
    }

    @Test
    public void getProductByTypes() throws Exception {
        List<ProductTypeEntity> given = new ArrayList<>();
        given.add(new ProductTypeEntity(ProductTestData.PRODUCT_TYPE_UUID_0, ProductTestData.PRODUCT_NAME_0));
        given.add(new ProductTypeEntity(ProductTestData.PRODUCT_TYPE_UUID_1, ProductTestData.PRODUCT_NAME_1));
        given.add(new ProductTypeEntity(ProductTestData.PRODUCT_TYPE_UUID_2, ProductTestData.PRODUCT_NAME_2));


        List<ProductEntity> productEntities = new ArrayList<>();
        productEntities.add(new ProductEntity(PRODUCT_UUID_0,PRODUCT_NAME_0, PRODUCT_TYPE_UUID_0, PRODUCT_PRICE_0));
        productEntities.add(new ProductEntity(PRODUCT_UUID_1,PRODUCT_NAME_1, PRODUCT_TYPE_UUID_1, PRODUCT_PRICE_1));
        productEntities.add(new ProductEntity(PRODUCT_UUID_2,PRODUCT_NAME_2, PRODUCT_TYPE_UUID_2, PRODUCT_PRICE_2));

        Observable<List<ProductEntity>> productByTypes = getProductsByTypesUseCase.getProductByTypes(given);

        TestObserver<List<ProductEntity>> listTestObserver = new TestObserver<>();
        productByTypes.subscribe(listTestObserver);
        listTestObserver.assertNoErrors();
        List<List<Object>> events = listTestObserver.getEvents();
        List<ProductEntity> actual = (List<ProductEntity>) events.get(0).get(0);
        Assert.assertEquals(productEntities, actual);

    }

}