package com.umbaba.shopper;


import com.umbaba.entity.ProductEntity;

import java.util.ArrayList;
import java.util.List;

public class ProductTestData {
    public static final String PRODUCT_UUID_0 = "ca77e48d-0000-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_1 = "ca77e48d-1111-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_2 = "ca77e48d-2222-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_3 = "ca77e48d-3333-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_4 = "ca77e48d-4444-4c8f-b754-9bbbe2704266";


    public static final String PRODUCT_NAME_0 = "name0";
    public static final String PRODUCT_NAME_1 = "name1";
    public static final String PRODUCT_NAME_2 = "name2";
    public static final String PRODUCT_NAME_3 = "name3";
    public static final String PRODUCT_NAME_4 = "name4";

    public static final int PRODUCT_PRICE_0 = 5;
    public static final int PRODUCT_PRICE_1 = 10;
    public static final int PRODUCT_PRICE_2 = 20;
    public static final int PRODUCT_PRICE_3 = 30;
    public static final int PRODUCT_PRICE_4 = 40;


    public static final String PRODUCT_TYPE_UUID_0 = "zx45a57f-0000-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_1 = "zx45a57f-1111-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_2 = "zx45a57f-2222-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_3 = "zx45a57f-3333-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_4 = "zx45a57f-4444-4c8f-b754-9bbbe2704266";

    public static final List<ProductEntity> PRODUCT_ENTITIES = new ArrayList<>();

    static{
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_0,PRODUCT_NAME_0, PRODUCT_TYPE_UUID_0, PRODUCT_PRICE_0));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_1,PRODUCT_NAME_1, PRODUCT_TYPE_UUID_1, PRODUCT_PRICE_1));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_2,PRODUCT_NAME_2, PRODUCT_TYPE_UUID_2, PRODUCT_PRICE_2));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_3,PRODUCT_NAME_3, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_4,PRODUCT_NAME_4, PRODUCT_TYPE_UUID_4, PRODUCT_PRICE_4));
    }

}
