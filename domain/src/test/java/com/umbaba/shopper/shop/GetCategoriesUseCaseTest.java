package com.umbaba.shopper.shop;

import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.boundries.ICategoryGateway;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;


public class GetCategoriesUseCaseTest {
    @Mock
    ICategoryGateway categoryGateway;
    private GetCategoriesUseCase getCategoriesUseCase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        getCategoriesUseCase = new GetCategoriesUseCase(categoryGateway);
    }

    @Test
    public void categories() throws Exception {
        getCategoriesUseCase.categories();
        Mockito.verify(categoryGateway).categories();
    }

}