package com.umbaba.shopper.boundries;


import com.umbaba.entity.ProductEntity;
import com.umbaba.entity.ProductTypeEntity;

import java.util.List;

import io.reactivex.Observable;

public interface IProductsGateway {
    Observable<List<ProductEntity>> products();

}
