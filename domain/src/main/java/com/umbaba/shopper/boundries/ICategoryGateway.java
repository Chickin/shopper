package com.umbaba.shopper.boundries;


import com.umbaba.entity.CategoryEntity;

import java.util.List;

import io.reactivex.Observable;

public interface ICategoryGateway {
    Observable<List<CategoryEntity>> categories();

}
