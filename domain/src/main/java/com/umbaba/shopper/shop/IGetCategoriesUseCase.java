package com.umbaba.shopper.shop;


import com.umbaba.entity.CategoryEntity;

import java.util.List;

import io.reactivex.Observable;

public interface IGetCategoriesUseCase {
    Observable<List<CategoryEntity>> categories();
}
