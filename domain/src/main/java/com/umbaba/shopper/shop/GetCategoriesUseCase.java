package com.umbaba.shopper.shop;


import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.boundries.ICategoryGateway;

import java.util.List;

import io.reactivex.Observable;

public class GetCategoriesUseCase implements IGetCategoriesUseCase {

    ICategoryGateway categoryGateway;

    public GetCategoriesUseCase(ICategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        return categoryGateway.categories();
    }
}
