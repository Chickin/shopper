package com.umbaba.shopper.order;


import com.umbaba.entity.ProductEntity;
import com.umbaba.entity.ProductTypeEntity;

import java.util.List;

import io.reactivex.Observable;

public interface IGetProductsByTypesUseCase {
    Observable<List<ProductEntity>> getProductByTypes(List<ProductTypeEntity> productTypes);
}
