package com.umbaba.shopper.order;


import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.umbaba.entity.ProductEntity;
import com.umbaba.entity.ProductTypeEntity;
import com.umbaba.shopper.boundries.IProductsGateway;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class GetProductsByTypesUseCase implements IGetProductsByTypesUseCase {

    IProductsGateway productsGateway;

    public GetProductsByTypesUseCase(IProductsGateway productsGateway) {
        this.productsGateway = productsGateway;
    }

    @Override
    @RxLogObservable
    public Observable<List<ProductEntity>> getProductByTypes(List<ProductTypeEntity> productTypes) {
        Single<List<String>> selection = Observable.fromIterable(productTypes)
                .map(ProductTypeEntity::getId)
                .toList();
        List<String> strings = selection.blockingGet();

        return productsGateway
                .products()
                .flatMapIterable(productEntities -> productEntities)
                .filter(productEntity -> {
                    String productTypeUUID = productEntity.getProductTypeUUID();
                    return strings.contains(productTypeUUID);
                })
                .toList().toObservable();
//        return productsGateway.products().filter(productEntities -> Observable.fromIterable(productTypes).contains(productEntities).blockingGet());
    }

}
