package com.umbaba.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
@EqualsAndHashCode
@Getter
public class ProductEntity {

    private final String uuid;
    private final String name;
    private final String productTypeUUID;
    private final int price;

    public ProductEntity(String uuid, String name, String productTypeUUID, int price) {
        this.uuid = uuid;
        this.name = name;
        this.productTypeUUID = productTypeUUID;
        this.price = price;
    }

}
