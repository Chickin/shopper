package com.umbaba.entity;



public class ProductTypeEntity {

    private final String uuid;
    private final String name;

    public ProductTypeEntity(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public String getId() {
        return uuid;
    }
}
