package com.umbaba.entity;


import java.util.ArrayList;

public class CategoryEntity {
    private String name;
    private String imageUrl;
    private long lastUpdated;
    private ArrayList<ProductTypeEntity> products;

    public CategoryEntity() {
    }

    public CategoryEntity(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<ProductTypeEntity> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductTypeEntity> products) {
        this.products = products;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntity that = (CategoryEntity) o;

        if (lastUpdated != that.lastUpdated) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        return products != null ? products.equals(that.products) : that.products == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (int) (lastUpdated ^ (lastUpdated >>> 32));
        result = 31 * result + (products != null ? products.hashCode() : 0);
        return result;
    }
}
