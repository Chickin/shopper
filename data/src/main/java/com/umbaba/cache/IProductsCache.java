package com.umbaba.cache;


import com.umbaba.entity.ProductEntity;

import java.util.List;

import io.reactivex.Observable;

public interface IProductsCache extends ICache {

    Observable<List<ProductEntity>> get();

    void put(List<ProductEntity> productEntities);
}