package com.umbaba.cache;


import java.util.concurrent.TimeUnit;

public interface ICache {
    long EXPIRATION_PERIOD = TimeUnit.SECONDS.toMillis(4);

    boolean isExpired();

    boolean isCached();
}
