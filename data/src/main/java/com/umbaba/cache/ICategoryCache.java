package com.umbaba.cache;


import com.umbaba.entity.CategoryEntity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public interface ICategoryCache extends ICache {

    Observable<List<CategoryEntity>> get();

    void put(List<CategoryEntity> townshipEntities);
}