package com.umbaba.shopper.data.repository.datasource;


import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.boundries.ICategoryGateway;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class CategoryCloudDataStore implements ICategoryGateway {
    private ICategoryCache categoryCache;

    public CategoryCloudDataStore(ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        List<CategoryEntity> categoryEntities = new ArrayList<>();
        categoryEntities.add(new CategoryEntity("asd", "asd"));
        categoryEntities.add(new CategoryEntity("asd", "asd"));
        categoryEntities.add(new CategoryEntity("asd", "asd"));
        return Observable.fromArray(categoryEntities).doOnNext(list-> categoryCache.put(list));
    }
}
