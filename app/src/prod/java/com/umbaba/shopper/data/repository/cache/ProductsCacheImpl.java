package com.umbaba.shopper.data.repository.cache;

import com.umbaba.cache.ICategoryCache;
import com.umbaba.cache.IProductsCache;
import com.umbaba.entity.CategoryEntity;
import com.umbaba.entity.ProductEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class ProductsCacheImpl implements IProductsCache{


    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public boolean isCached() {
        return false;
    }

    @Override
    public Observable<List<ProductEntity>> get() {
        return null;
    }

    @Override
    public void put(List<ProductEntity> productEntities) {

    }
}
