package com.umbaba.shopper.data.repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.umbaba.entity.CategoryEntity;
import com.umbaba.entity.ProductEntity;
import com.umbaba.shopper.data.repository.datasource.CategoryLocalDataStore;
import com.umbaba.shopper.data.repository.datasource.ProductLocalDataStore;

@Database(entities = {CategoryEntity.class, ProductEntity.class}, version = 1)
public abstract class AppDB extends RoomDatabase {
    public abstract CategoryLocalDataStore categoryDatestore();

    public abstract ProductLocalDataStore productDatestore();
}
