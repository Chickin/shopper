package com.umbaba.shopper.data.repository.cache;

import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class CategoryCacheImpl implements ICategoryCache {

    List<CategoryEntity> categoryEntities = new ArrayList<>();


    @Override
    public boolean isExpired() {
        return System.currentTimeMillis() - categoryEntities.get(0).getLastUpdated() > EXPIRATION_PERIOD;
    }

    @Override
    public boolean isCached() {
        return categoryEntities != null && categoryEntities.size() > 0;
    }

    @Override
    public Observable<List<CategoryEntity>> get() {
        return Observable.fromArray(categoryEntities);
    }

    @Override
    public void put(List<CategoryEntity> categoryEntities) {
        this.categoryEntities = categoryEntities;
    }
}
