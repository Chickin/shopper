package com.umbaba.shopper.data.repository.datasource;


import com.umbaba.cache.IProductsCache;
import com.umbaba.entity.ProductEntity;
import com.umbaba.shopper.boundries.IProductsGateway;

import java.util.List;

import io.reactivex.Observable;

public class ProductCloudDataStore implements IProductsGateway {

    private final IProductsCache productsCache;

    public ProductCloudDataStore(IProductsCache productsCache) {
        this.productsCache = productsCache;
    }

    @Override
    public Observable<List<ProductEntity>> products() {
        return null;
    }
}
