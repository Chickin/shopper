package com.umbaba.shopper.presentation.menu.presenter;

import com.umbaba.shopper.presentation.menu.model.IMenuItemsSource;
import com.umbaba.shopper.presentation.menu.model.MenuItemsSource;
import com.umbaba.shopper.presentation.menu.view.IMenuView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

public class MenuPresenterTest {
    @Mock
    IMenuItemsSource menuItemsSource;

    @Mock
    IMenuView menuView;

    private MenuPresenter menuPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        menuPresenter = new MenuPresenter(menuItemsSource);
    }

    @Test
    public void start() throws Exception {
        menuPresenter.start();
        Mockito.verify(menuItemsSource).loadItems();
    }

}