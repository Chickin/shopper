package com.umbaba.shopper.data;

import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.shopper.data.repository.cache.CategoryCacheImpl;
import com.umbaba.shopper.data.repository.datasource.CategoryCloudDataStore;
import com.umbaba.shopper.data.repository.datasource.CategoryLocalDataStore;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CategoryDataStoreFactoryTest {
    @Mock
    CategoryCacheImpl categoryCache;
    private CategoryDataStoreFactory categoryDataStoreFactory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        categoryDataStoreFactory = new CategoryDataStoreFactory(categoryCache);
    }

    @Test
    public void isCachedCalled() throws Exception {
        Mockito.when(categoryCache.isCached()).thenReturn(true);
        categoryDataStoreFactory.create();
        Mockito.verify(categoryCache).isCached();
    }
    @Test
    public void isExpiredCalled() throws Exception {
        Mockito.when(categoryCache.isCached()).thenReturn(true);
        categoryDataStoreFactory.create();
        Mockito.verify(categoryCache).isExpired();
    }

    @Test
    public void createLocal() throws Exception {
        Mockito.when(categoryCache.isCached()).thenReturn(true);
        Mockito.when(categoryCache.isExpired()).thenReturn(false);

        ICategoryGateway categoryGateway = categoryDataStoreFactory.create();
        Assert.assertTrue(categoryGateway instanceof CategoryLocalDataStore);
    }

    @Test
    public void createRemoteNotCached() throws Exception {
        Mockito.when(categoryCache.isCached()).thenReturn(false);

        ICategoryGateway categoryGateway = categoryDataStoreFactory.create();
        Assert.assertTrue(categoryGateway instanceof CategoryCloudDataStore);
    }
    @Test
    public void createRemoteExpired() throws Exception {
        Mockito.when(categoryCache.isExpired()).thenReturn(true);

        ICategoryGateway categoryGateway = categoryDataStoreFactory.create();
        Assert.assertTrue(categoryGateway instanceof CategoryCloudDataStore);
    }

}