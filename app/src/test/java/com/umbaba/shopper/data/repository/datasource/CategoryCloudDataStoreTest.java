package com.umbaba.shopper.data.repository.datasource;

import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.data.repository.cache.CategoryCacheImpl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.*;

public class CategoryCloudDataStoreTest {
    @Mock
    CategoryCacheImpl categoryCache;
    private CategoryCloudDataStore categoryCloudDataStore;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        categoryCloudDataStore = new CategoryCloudDataStore(categoryCache);
    }

    @Test
    public void categories() throws Exception {
        TestObserver<List<CategoryEntity>> categoryEntityTestSubscriber = new TestObserver<>();
        categoryCloudDataStore.categories().subscribe(categoryEntityTestSubscriber);

        categoryEntityTestSubscriber.assertNoErrors();
        categoryEntityTestSubscriber.assertComplete();
    }
  @Test
    public void categoriesPutToCache() throws Exception {
        TestObserver<List<CategoryEntity>> categoryEntityTestSubscriber = new TestObserver<>();
        categoryCloudDataStore.categories().subscribe(categoryEntityTestSubscriber);
        Mockito.verify(categoryCache).put(Mockito.anyList());

        categoryEntityTestSubscriber.assertNoErrors();
        categoryEntityTestSubscriber.assertComplete();
    }

}