package com.umbaba.shopper.data.repository;


import com.umbaba.entity.CategoryEntity;
import com.umbaba.entity.ProductTypeEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class TestData {

    public static ArrayList<CategoryEntity> CATEGORIES;


    public static final String CATEGORY_0 = "categ0";
    public static final String CATEGORY_1 = "categ1";
    public static final String CATEGORY_2 = "categ2";
    public static final String CATEGORY_3 = "categ3";

    public static final String PRODUCT = "product";
    public static final String PRODUCT1 = "product1";
    public static final String PRODUCT2 = "product2";
    public static final String PRODUCT3 = "product3";

    public static final UUID PR_2_UUID = UUID.randomUUID();
    public static final UUID PR_3_UUID = UUID.randomUUID();
    public static final UUID PR_0_UUID = UUID.randomUUID();

    public static final UUID PR_1_UUID = UUID.randomUUID();

    public static Date EXPIRED_DATE;

    static {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        EXPIRED_DATE = cal.getTime();


        CATEGORIES = new ArrayList<>();
        ProductTypeEntity product = new ProductTypeEntity(PR_0_UUID.toString(), PRODUCT);
        ProductTypeEntity product1 = new ProductTypeEntity(PR_1_UUID.toString(), PRODUCT1);
        ProductTypeEntity product2 = new ProductTypeEntity(PR_2_UUID.toString(), PRODUCT2);
        ProductTypeEntity product3 = new ProductTypeEntity(PR_3_UUID.toString(), PRODUCT3);

        ArrayList<ProductTypeEntity> productEntities = new ArrayList<>();
        productEntities.add(product);
        productEntities.add(product1);
        productEntities.add(product2);
        productEntities.add(product3);

        CategoryEntity categoryEntity = new CategoryEntity(CATEGORY_0, "img");
        categoryEntity.setProducts(productEntities);
        categoryEntity.setLastUpdated(EXPIRED_DATE.getTime());
        CATEGORIES.add(categoryEntity);

        CategoryEntity categoryEntity1 = new CategoryEntity(CATEGORY_1, "img");
        categoryEntity1.setProducts(productEntities);
        categoryEntity1.setLastUpdated(EXPIRED_DATE.getTime());
        CATEGORIES.add(categoryEntity1);

        CategoryEntity categoryEntity2 = new CategoryEntity(CATEGORY_2, "img");
        categoryEntity2.setProducts(productEntities);
        categoryEntity2.setLastUpdated(EXPIRED_DATE.getTime());
        CATEGORIES.add(categoryEntity2);

        CategoryEntity categoryEntity3 = new CategoryEntity(CATEGORY_3, "img");
        categoryEntity3.setProducts(productEntities);
        categoryEntity3.setLastUpdated(EXPIRED_DATE.getTime());
        CATEGORIES.add(categoryEntity3);
    }


}
