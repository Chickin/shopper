package com.umbaba.shopper.data.repository.cache;

import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.data.repository.TestData;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.*;

public class CategoryCacheImplTest {

    private CategoryCacheImpl categoryCache;

    @Before
    public void setUp() throws Exception {
        categoryCache = new CategoryCacheImpl();
    }

    @Test
    public void isCached() throws Exception {
        categoryCache.put(null);
        boolean cached = categoryCache.isCached();
        Assert.assertFalse(cached);
    }

    @Test
    public void isExpired() throws Exception {
        categoryCache.put(TestData.CATEGORIES);
        boolean expired = categoryCache.isExpired();
        Assert.assertTrue(expired);
    }

    @Test
    public void putAndGet() throws Exception {
        categoryCache.put(TestData.CATEGORIES);
        TestObserver<List<CategoryEntity>> listTestObserver = new TestObserver<>();
        Observable<List<CategoryEntity>> observable = categoryCache.get();

        observable.subscribe(listTestObserver);
        listTestObserver.assertValues(TestData.CATEGORIES);
        listTestObserver.assertComplete();
    }

}