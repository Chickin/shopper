package com.umbaba.shopper.navigation;

import android.content.Context;
import android.content.Intent;

import com.umbaba.shopper.presentation.shop.model.ProductType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;

public class StartOrderUseCaseTest {

    @Mock
    Context context;
    private StartOrderUseCase startOrderUseCase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        startOrderUseCase = new StartOrderUseCase(context);
    }

    @Test
    public void execute() throws Exception {
        ArrayList<ProductType> productTypes = new ArrayList<>();
        Intent starterIntent = Mockito.mock(Intent.class);
        startOrderUseCase.execute(productTypes, starterIntent);
        Mockito.verify(starterIntent).putParcelableArrayListExtra(StartOrderUseCase.EXTRA_DATA, productTypes);
        Mockito.verify(context).startActivity(any());
    }

}