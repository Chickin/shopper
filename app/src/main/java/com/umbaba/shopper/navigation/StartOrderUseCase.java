package com.umbaba.shopper.navigation;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.umbaba.shopper.presentation.order.view.OrderActivity;
import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.ArrayList;

import javax.inject.Inject;

public class StartOrderUseCase implements IStartOrderUseCase {
    private Context context;

    public static final String EXTRA_DATA = "EXTRA_DATA";

    @Inject
    public StartOrderUseCase(Context context) {
        this.context = context;
    }

    @Override
    public void execute(ArrayList<ProductType> selectedProductTypes, Intent starterIntent) {
        starterIntent.setComponent(new ComponentName(context, OrderActivity.class));
        starterIntent.putParcelableArrayListExtra(EXTRA_DATA, selectedProductTypes);
        context.startActivity(starterIntent);
    }
}
