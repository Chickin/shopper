package com.umbaba.shopper.navigation;


import android.content.Intent;

import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.ArrayList;

public interface IStartOrderUseCase {

    void execute(ArrayList<ProductType> selectedProductTypes, Intent starterIntent);
}
