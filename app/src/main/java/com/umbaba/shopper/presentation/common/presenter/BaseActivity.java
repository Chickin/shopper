package com.umbaba.shopper.presentation.common.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.umbaba.shopper.ShopperApp;
import com.umbaba.shopper.presentation.common.view.IView;
import com.umbaba.shopper.presentation.di.component.AppComponent;
import com.umbaba.shopper.presentation.di.component.PresenterCache;
import com.umbaba.shopper.presentation.di.component.PresenterComponent;

import javax.inject.Inject;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IView{

    @Inject
    protected PresenterCache presenterCache;

    protected PresenterComponent<T> presenterComponent;

    protected T presenter;

    protected AppComponent getApplicationComponent() {
        return ShopperApp.getDaggerAppComponent();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        initBinding();
        restoreOrCreatePresenter();
        presenter.activityCreated(getIntent());
    }

    protected abstract void inject();

    protected abstract void initBinding();


    @SuppressWarnings("unchecked")
    private void restoreOrCreatePresenter() {
        presenter = presenterCache.getPresenter(getClass().getName());
        if (presenter == null) {
            presenter = presenterComponent.getPresenter();
            presenterCache.putPresenter(getClass().getName(), presenter);
        }
        presenter.bindView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onRestart() {
        super.onRestart();
        presenterCache.putPresenter(getClass().getName(), presenter);
        presenter.bindView(this);
        presenter.restart();
    }

    @Override
    protected void onStop() {
        if (!isChangingConfigurations()) {
            presenterCache.removePresenter(presenter);
        }
        presenter.stop(isChangingConfigurations());
        super.onStop();
    }
}
