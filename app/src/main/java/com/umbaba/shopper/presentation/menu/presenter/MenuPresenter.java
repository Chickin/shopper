package com.umbaba.shopper.presentation.menu.presenter;


import android.content.Intent;
import android.view.View;

import com.umbaba.shopper.presentation.common.presenter.BasePresenter;
import com.umbaba.shopper.presentation.menu.model.AppMenuItem;
import com.umbaba.shopper.presentation.common.presenter.IPresenter;
import com.umbaba.shopper.presentation.menu.model.IMenuItemsSource;
import com.umbaba.shopper.presentation.menu.view.IMenuView;
import com.umbaba.shopper.presentation.menu.view.AppItemAdapter;

import java.util.List;

public class MenuPresenter extends BasePresenter<IMenuView> implements  AppItemAdapter.ClickListener {
    private IMenuItemsSource menuItemsSource;
    private List<AppMenuItem> itemViewList;

    public MenuPresenter(IMenuItemsSource menuItemsSource) {
        this.menuItemsSource = menuItemsSource;
    }

    @Override
    public void start() {
        itemViewList = menuItemsSource.loadItems();
        getView().displayAppMenuItems(itemViewList);
    }

    @Override
    public void stop(boolean changingConfigurations) {

    }

    @Override
    public void onItemClicked(View v, int position) {
        AppMenuItem appMenuItem = itemViewList.get(position);
        appMenuItem.getAction().perform();

    }
}
