package com.umbaba.shopper.presentation.menu.model;


import android.support.annotation.ColorInt;

import com.umbaba.shopper.presentation.menu.model.action.Action;

public class AppMenuItem {
    private final String name;
    private final int bgColor;
    private final Action action;

    AppMenuItem(@ColorInt int bgColor, String name, Action action) {
        this.name = name;
        this.bgColor = bgColor;
        this.action = action;
    }

    public Action getAction() {
        return action;
    }

    public int getBgColor() {
        return bgColor;
    }

    public String getName() {
        return name;
    }

}
