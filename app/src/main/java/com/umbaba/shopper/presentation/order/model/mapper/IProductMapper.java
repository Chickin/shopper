package com.umbaba.shopper.presentation.order.model.mapper;


import com.umbaba.entity.ProductEntity;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;

public interface IProductMapper {
    ProductDisplayModel transform(ProductEntity productEntity);
}
