package com.umbaba.shopper.presentation.menu.view;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;

import com.umbaba.shopper.R;
import com.umbaba.shopper.databinding.ActivityMainBinding;
import com.umbaba.shopper.presentation.common.presenter.BaseActivity;
import com.umbaba.shopper.presentation.menu.di.DaggerMenuComponent;
import com.umbaba.shopper.presentation.menu.di.MenuModule;
import com.umbaba.shopper.presentation.menu.model.AppMenuItem;
import com.umbaba.shopper.presentation.menu.presenter.MenuPresenter;

import java.util.List;

public class MainActivity extends BaseActivity<MenuPresenter> implements IMenuView {

    private AppItemAdapter menuItemAdapter;
    private ActivityMainBinding binding;

    @Override
    protected void inject() {
        presenterComponent = DaggerMenuComponent.builder()
                .appComponent(getApplicationComponent())
                .menuModule(new MenuModule(this, this))
                .build();
        ((DaggerMenuComponent) presenterComponent).inject(this);
    }

    @Override
    protected void initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        menuItemAdapter = new AppItemAdapter(this);
        binding.recycler.setAdapter(menuItemAdapter);
        binding.recycler.setLayoutManager(new LinearLayoutManager(this));
    }



    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
        menuItemAdapter.setOnItemClickListener(presenter);
    }

    @Override
    public void displayAppMenuItems(List<AppMenuItem> appMenuItemList) {
        menuItemAdapter.setAppItems(appMenuItemList);
    }
}
