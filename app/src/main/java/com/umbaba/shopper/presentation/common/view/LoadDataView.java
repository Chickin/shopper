package com.umbaba.shopper.presentation.common.view;

public interface LoadDataView {

    void showLoading();

    void hideLoading();

    void showError(String errorMessage);
}
