package com.umbaba.shopper.presentation.menu.model;


import java.util.List;

public interface IMenuItemsSource {
    List<AppMenuItem> loadItems();
}
