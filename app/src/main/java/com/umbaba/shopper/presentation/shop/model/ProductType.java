package com.umbaba.shopper.presentation.shop.model;


import android.os.Parcel;
import android.os.Parcelable;


public class ProductType implements Parcelable {
    private String id;
    private String name;

    public ProductType(String productTypeUUID) {
        this.id = productTypeUUID;
    }


    public ProductType(String uuid, String name) {
        this.name = name;
        this.id = uuid;
    }

    public ProductType(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductType that = (ProductType) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductType> CREATOR = new Creator<ProductType>() {
        @Override
        public ProductType createFromParcel(Parcel in) {
            return new ProductType(in);
        }

        @Override
        public ProductType[] newArray(int size) {
            return new ProductType[size];
        }
    };
}
