package com.umbaba.shopper.presentation.order.di;

import com.umbaba.cache.IProductsCache;
import com.umbaba.shopper.boundries.IProductsGateway;
import com.umbaba.shopper.data.IDataStoreFactory;
import com.umbaba.shopper.data.ProductDataStoreFactory;
import com.umbaba.shopper.data.repository.cache.ProductsCacheImpl;
import com.umbaba.shopper.order.GetProductsByTypesUseCase;
import com.umbaba.shopper.order.IGetProductsByTypesUseCase;
import com.umbaba.shopper.presentation.order.model.mapper.IProductMapper;
import com.umbaba.shopper.presentation.order.model.mapper.ProductMapper;
import com.umbaba.shopper.presentation.order.vm.OrderVMFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class OrderModule {
    @Provides
    @OrderScreen
    IProductMapper productMapper() {
        return new ProductMapper();
    }


    @Provides
    @OrderScreen
    IProductsCache cache() {
        return new ProductsCacheImpl();
    }

    @Provides
    @OrderScreen
    IDataStoreFactory<IProductsGateway> gatewayIDataStoreFactory(IProductsCache cache) {
        return new ProductDataStoreFactory(cache);
    }

    @Provides
    @OrderScreen
    IProductsGateway productsGateway(IDataStoreFactory<IProductsGateway> factory) {
        return factory.create();
    }

    @Provides
    @OrderScreen
    IGetProductsByTypesUseCase getProductsByTypesUseCase(IProductsGateway productsGateway) {
        return new GetProductsByTypesUseCase(productsGateway);
    }

    @Provides
    @OrderScreen
    OrderVMFactory orderVMFactory(IProductMapper productMapper, IGetProductsByTypesUseCase getProductsByTypesUseCase) {
        return new OrderVMFactory(productMapper, getProductsByTypesUseCase);
    }
}
