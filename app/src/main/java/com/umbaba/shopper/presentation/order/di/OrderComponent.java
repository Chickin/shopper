package com.umbaba.shopper.presentation.order.di;

import com.umbaba.shopper.presentation.di.component.AppComponent;
import com.umbaba.shopper.presentation.order.view.OrderActivity;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = OrderModule.class)
@OrderScreen
public interface OrderComponent {

    void inject(OrderActivity orderActivity);
}
