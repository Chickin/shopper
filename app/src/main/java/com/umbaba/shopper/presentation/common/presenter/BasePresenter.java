package com.umbaba.shopper.presentation.common.presenter;


import android.arch.lifecycle.ViewModel;
import android.content.Intent;

import com.umbaba.shopper.presentation.common.view.IView;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<T extends IView> extends ViewModel implements IPresenter {

    private WeakReference<T> viewReference;
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();


    void bindView(T view) {
        if (viewReference != null && viewReference.get() == view) {
            return;
        }
        this.viewReference = new WeakReference<>(view);
    }

    protected T getView() {
        if (viewReference == null) {
            return null;
        }
        return viewReference.get();
    }

    @Override
    public void stop(boolean changingConfigurations) {
        if (!changingConfigurations) {
            compositeDisposable.clear();
        }
        viewReference = null;
    }

    protected void activityCreated(Intent intent){

    };

    protected void restart(){

    };
}
