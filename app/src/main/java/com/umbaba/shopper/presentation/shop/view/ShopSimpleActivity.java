package com.umbaba.shopper.presentation.shop.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.thoughtbot.expandablecheckrecyclerview.listeners.OnCheckChildClickListener;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.umbaba.shopper.R;
import com.umbaba.shopper.ShopperApp;
import com.umbaba.shopper.databinding.ActivityShopSimpleBinding;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.shop.di.DaggerShopComponent;
import com.umbaba.shopper.presentation.shop.di.ShopComponent;
import com.umbaba.shopper.presentation.shop.di.ShopModule;
import com.umbaba.shopper.presentation.shop.presenter.ShopViewModel;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategory;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategoryAdapter;
import com.umbaba.shopper.presentation.shop.vm.ShopViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


public class ShopSimpleActivity extends AppCompatActivity {

    private static final String TAG = "ShopSimpleActivity";

    private ActivityShopSimpleBinding binding;
    private MultiCheckCategoryAdapter adapter;

    @Inject
    LoadingState loadingState;

    @Inject
    ShopViewModelFactory factory;

    private ShopViewModel shopViewModel;
    private ShopComponent shopComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        initBinding();
        if (savedInstanceState == null) {
            shopViewModel.start();
        }
    }

    protected void inject() {
        this.shopComponent = DaggerShopComponent.builder()
                .shopModule(new ShopModule(this))
                .appComponent(ShopperApp.getDaggerAppComponent())
                .build();
        shopComponent.inject(this);
    }

    protected void initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_simple);
        binding.setState(loadingState);
        shopViewModel = ViewModelProviders.of(this, factory).get(ShopViewModel.class);
        shopViewModel.getCategoriesList().observe(this, this::displayCategories);
        binding.setViewModel(shopViewModel);
    }


    public void displayCategories(List<MultiCheckCategory> multiCheckCategories) {
        adapter = new MultiCheckCategoryAdapter(multiCheckCategories);
        Observable<CheckedExpandableGroup> observableSelection = getObservableSelection(adapter);
        shopViewModel.subscribeOnSelection(observableSelection);
        binding.recycler.setLayoutManager(new LinearLayoutManager(this));
        binding.recycler.setAdapter(adapter);
        binding.recycler.setItemAnimator(null);
    }

    Observable<CheckedExpandableGroup> getObservableSelection(final MultiCheckCategoryAdapter adapter) {
        return Observable.create(e -> {
            OnCheckChildClickListener listener = (view, b, checkedExpandableGroup, i) -> {
                e.onNext(checkedExpandableGroup);
                Log.d(TAG, "getObservableSelection: i=" + i + " " + checkedExpandableGroup.selectedChildren[0] + "  " + checkedExpandableGroup.selectedChildren[1]);
            };
            adapter.setChildClickListener(listener);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        shopComponent = null;
    }
}
