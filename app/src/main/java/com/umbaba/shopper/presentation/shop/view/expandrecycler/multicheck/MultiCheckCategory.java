package com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck;

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;
import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.List;

public class MultiCheckCategory extends MultiCheckExpandableGroup {

  private String imagePath;

  public MultiCheckCategory(String title, List<ProductType> items, String imagePath) {
    super(title, items);
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }
}

