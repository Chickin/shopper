package com.umbaba.shopper.presentation.common.presenter;


public interface IPresenter {
    void start();

    void stop(boolean changingConfigurations);
}
