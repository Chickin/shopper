package com.umbaba.shopper.presentation.di.component;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.umbaba.shopper.presentation.common.presenter.BasePresenter;

import java.util.Map;


public final class PresenterCache {


    private final Map<String, BasePresenter> cachedPresenters = new ArrayMap<>();

    @SuppressWarnings("unchecked")
    @Nullable
    public <T> T getPresenter(String activityName) {
        return (T) cachedPresenters.get(activityName);
    }

    public void putPresenter(String activityName, BasePresenter presenter) {
        cachedPresenters.put(activityName, presenter);
    }


    public void removePresenter(BasePresenter presenterToRemove) {
        for (Map.Entry<String, BasePresenter> entry : cachedPresenters.entrySet()) {
            if (presenterToRemove.equals(entry.getValue())) {
                cachedPresenters.remove(entry.getKey());
                break;
            }
        }
    }
}
