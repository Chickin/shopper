package com.umbaba.shopper.presentation.order.model.mapper;


import com.umbaba.entity.ProductEntity;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;
import com.umbaba.shopper.presentation.shop.model.ProductType;

public class ProductMapper implements IProductMapper {

    @Override
    public ProductDisplayModel transform(ProductEntity productEntity) {
        return new ProductDisplayModel(productEntity.getUuid(), productEntity.getName(), productEntity.getPrice(), new ProductType(productEntity.getProductTypeUUID()));
    }
}
