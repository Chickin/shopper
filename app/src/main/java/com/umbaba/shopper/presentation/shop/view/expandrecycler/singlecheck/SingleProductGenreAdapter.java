package com.umbaba.shopper.presentation.shop.view.expandrecycler.singlecheck;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.umbaba.shopper.R;
import com.umbaba.shopper.presentation.shop.model.ProductType;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.CategoryViewHolder;

import java.util.List;

public class SingleProductGenreAdapter extends
        CheckableChildRecyclerViewAdapter<CategoryViewHolder, SingleCheckProductViewHolder> {

  public SingleProductGenreAdapter(List<SingleCheckCategory> groups) {
    super(groups);
  }

  @Override
  public SingleCheckProductViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_item_singlecheck_arist, parent, false);
    return new SingleCheckProductViewHolder(view);
  }

  @Override
  public void onBindCheckChildViewHolder(SingleCheckProductViewHolder holder, int position,
                                         CheckedExpandableGroup group, int childIndex) {
    final ProductType artist = (ProductType) group.getItems().get(childIndex);
    holder.setArtistName(artist.getName());
  }

  @Override
  public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_item_genre, parent, false);
    return new CategoryViewHolder(view);
  }

  @Override
  public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition,
      ExpandableGroup group) {
    holder.setCategoryTitle(group);
  }
}
