package com.umbaba.shopper.presentation.menu.view;


import com.umbaba.shopper.presentation.common.view.IView;
import com.umbaba.shopper.presentation.menu.model.AppMenuItem;

import java.util.List;

public interface IMenuView extends IView{

    void displayAppMenuItems(List<AppMenuItem> appMenuItemList);

}
