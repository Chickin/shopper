package com.umbaba.shopper.presentation.menu.di;

import com.umbaba.shopper.presentation.di.component.AppComponent;
import com.umbaba.shopper.presentation.di.component.PresenterCache;
import com.umbaba.shopper.presentation.di.component.PresenterComponent;
import com.umbaba.shopper.presentation.menu.presenter.MenuPresenter;
import com.umbaba.shopper.presentation.menu.view.MainActivity;

import dagger.Component;

@SuppressWarnings("WeakerAccess")
@MenuScope
@Component(modules = MenuModule.class, dependencies = AppComponent.class)
public interface MenuComponent extends PresenterComponent<MenuPresenter> {
    void inject(MainActivity activity);

    PresenterCache presenterCache();
}
