package com.umbaba.shopper.presentation.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.umbaba.shopper.data.repository.AppDB;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.di.component.PresenterCache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class AppModule {

    private final Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    PresenterCache providePresenterCache() {
        return new PresenterCache();
    }

    @Provides
    @Singleton
    LoadingState loadingState() {
        return new LoadingState();
    }

    @Provides
    @Singleton
    AppDB db() {
        return Room.databaseBuilder(context,
                AppDB.class, "shopper").build();
    }
}
