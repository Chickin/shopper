package com.umbaba.shopper.presentation.di.component;


import com.umbaba.shopper.presentation.common.presenter.BasePresenter;

public interface PresenterComponent<T extends BasePresenter> {
    T getPresenter();
}
