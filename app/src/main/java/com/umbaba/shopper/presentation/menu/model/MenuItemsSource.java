package com.umbaba.shopper.presentation.menu.model;


import android.content.Context;

import com.github.bassaer.library.MDColor;
import com.umbaba.shopper.presentation.menu.model.action.StartActivityAction;
import com.umbaba.shopper.presentation.shop.view.ShopSimpleActivity;

import java.util.ArrayList;
import java.util.List;

public class MenuItemsSource implements IMenuItemsSource {


    private Context context;

    public MenuItemsSource(Context context) {
        this.context = context;
    }

    @Override
    public List<AppMenuItem> loadItems() {
        ArrayList<AppMenuItem> appMenuItems = new ArrayList<>();
        AppMenuItem menuItem = new AppMenuItem(MDColor.AMBER_50, "123",
                new StartActivityAction(context, ShopSimpleActivity.class));
        appMenuItems.add(menuItem);

        AppMenuItem appMenuItem = new AppMenuItem(MDColor.AMBER_100, "12233",
                new StartActivityAction(context, ShopSimpleActivity.class));
        appMenuItems.add(appMenuItem);

        AppMenuItem appMenuItem1 = new AppMenuItem(MDColor.AMBER_200, "234234",
                new StartActivityAction(context, ShopSimpleActivity.class));
        appMenuItems.add(appMenuItem1);

        AppMenuItem menuItem1 = new AppMenuItem(MDColor.AMBER_300, "234234234234",
                new StartActivityAction(context, ShopSimpleActivity.class));
        appMenuItems.add(menuItem1);
        return appMenuItems;
    }
}
