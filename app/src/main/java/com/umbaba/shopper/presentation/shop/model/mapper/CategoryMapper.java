package com.umbaba.shopper.presentation.shop.model.mapper;


import com.umbaba.entity.CategoryEntity;
import com.umbaba.entity.ProductTypeEntity;
import com.umbaba.shopper.presentation.shop.model.ProductType;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CategoryMapper implements ICategoryMapper {

    @Override
    public List<MultiCheckCategory> transformList(List<CategoryEntity> categoryEntities) {

        ArrayList<MultiCheckCategory> multiCheckCategories = new ArrayList<>();
        for (CategoryEntity category : categoryEntities) {
            List<ProductTypeEntity> productEntities = category.getProducts();
            List<ProductType> productTypes = new ArrayList<>();
            if (productEntities != null) {
                for (ProductTypeEntity productTypeEntity : productEntities) {
                    productTypes.add(new ProductType(productTypeEntity.getId(), productTypeEntity.getName()));
                }
            }
            multiCheckCategories.add(new MultiCheckCategory(category.getName(), productTypes, category.getImageUrl()));
        }
        return multiCheckCategories;
    }


}