package com.umbaba.shopper.presentation.order.vm;

import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.umbaba.shopper.order.IGetProductsByTypesUseCase;
import com.umbaba.shopper.presentation.order.model.mapper.IProductMapper;

import javax.inject.Inject;


public class OrderVMFactory implements ViewModelProvider.Factory {


    private final IProductMapper productMapper;
    private final IGetProductsByTypesUseCase getProductsByTypesUseCase;

    @Inject
    public OrderVMFactory(IProductMapper productMapper, IGetProductsByTypesUseCase getProductsByTypesUseCase) {
        this.productMapper = productMapper;
        this.getProductsByTypesUseCase = getProductsByTypesUseCase;
    }

    @NonNull
    @Override
    public OrderViewModel create(@NonNull Class modelClass) {
        return new OrderViewModel(productMapper, getProductsByTypesUseCase);
    }
}
