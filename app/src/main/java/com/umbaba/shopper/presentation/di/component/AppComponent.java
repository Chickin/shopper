package com.umbaba.shopper.presentation.di.component;


import com.umbaba.shopper.data.repository.AppDB;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = AppModule.class)
@Singleton
public interface AppComponent {

    PresenterCache presenterCache();

    LoadingState loadingState();

    AppDB db();
}
