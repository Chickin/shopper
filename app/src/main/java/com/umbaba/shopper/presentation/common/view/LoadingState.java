package com.umbaba.shopper.presentation.common.view;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingConversion;
import android.util.Log;
import android.view.View;

import com.umbaba.shopper.BR;

public class LoadingState extends BaseObservable {

    private static final String TAG = "LoadingState";

    @Bindable
    private boolean loading;

    @Bindable
    private String error;

    public void setLoading(boolean loading) {
        Log.d(TAG, "setLoading() called with: loading = [" + loading + "]");
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public void setError(String error) {
        Log.d(TAG, "setError() called with: error = [" + error + "]");
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    public boolean isLoading() {
        return loading;
    }

    public String getError() {
        return error;
    }

    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }

}
