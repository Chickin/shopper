package com.umbaba.shopper.presentation.shop.presenter;


import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.util.Log;

import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.umbaba.shopper.navigation.IStartOrderUseCase;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.shop.model.mapper.CategoryMapper;
import com.umbaba.shopper.presentation.shop.model.ProductType;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategory;
import com.umbaba.shopper.shop.IGetCategoriesUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;

public class ShopViewModel extends ViewModel implements IShopViewModel {
    private static final String TAG = "ShopViewModel";
    private ReplaySubject<ProductType> groupReplaySubject;
    private CategoryMapper categoryMapper;
    private IGetCategoriesUseCase getCategoriesUseCase;
    private IStartOrderUseCase startOrderUseCase;
    MutableLiveData<List<MultiCheckCategory>> categoriesList;
    private LoadingState loading;
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public ShopViewModel(IGetCategoriesUseCase getCategoriesUseCase,
                         CategoryMapper categoryMapper,
                         IStartOrderUseCase startOrderUseCase,
                         LoadingState loading) {
        this.categoryMapper = categoryMapper;
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.startOrderUseCase = startOrderUseCase;
        this.groupReplaySubject = ReplaySubject.create();
        this.categoriesList = new MutableLiveData<>();
        this.loading = loading;
    }


    public void start() {
        loadData();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
    }

    private void loadData() {
        compositeDisposable.add(getCategoriesUseCase.categories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(categoryMapper::transformList)
                .doOnSubscribe(disposable -> loading.setLoading(true))
                .doAfterTerminate(() -> loading.setLoading(false))
                .subscribe(
                        //next
                        (categories) -> categoriesList.postValue(categories),
                        //error
                        throwable -> loading.setError(throwable.getMessage())));
    }

    @Override
    public void subscribeOnSelection(Observable<CheckedExpandableGroup> observableSelection) {
        compositeDisposable.add(observableSelection.subscribe(
                (group) -> {
                    Log.d(TAG, "subscribeOn: " + group.getTitle() + " " + group.selectedChildren.length);
                    for (int i = 0; i < group.selectedChildren.length; i++) {
                        Log.d(TAG, "" + group.selectedChildren[i]);
                        if (group.selectedChildren[i]) {
                            List items = group.getItems();
                            groupReplaySubject.onNext(((ProductType) items.get(i)));
                        }
                    }
                }));
    }

    @Override
    public void pickSelection() {
        groupReplaySubject.onComplete();
        groupReplaySubject
                .distinct()
                .toList()
                .subscribe(products ->
                        startOrderUseCase
                                .execute(new ArrayList<>(products), new Intent()))
                .dispose();
    }

    public MutableLiveData<List<MultiCheckCategory>> getCategoriesList() {
        return categoriesList;
    }

}
