package com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.umbaba.shopper.R;
import com.umbaba.shopper.presentation.shop.model.ProductType;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.CategoryViewHolder;

import java.util.List;

public class MultiCheckCategoryAdapter extends
        CheckableChildRecyclerViewAdapter<CategoryViewHolder, MultiCheckProductViewHolder> {

  public MultiCheckCategoryAdapter(List<MultiCheckCategory> groups) {
    super(groups);
  }

  @Override
  public MultiCheckProductViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_item_multicheck_artist, parent, false);
    return new MultiCheckProductViewHolder(view);
  }

  @Override
  public void onBindCheckChildViewHolder(MultiCheckProductViewHolder holder, int position,
                                         CheckedExpandableGroup group, int childIndex) {
    final ProductType artist = (ProductType) group.getItems().get(childIndex);
    holder.setArtistName(artist.getName());
  }

  @Override
  public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_item_genre, parent, false);
    return new CategoryViewHolder(view);
  }

  @Override
  public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition,
                                    ExpandableGroup group) {
    holder.setCategoryTitle(group);
  }
}
