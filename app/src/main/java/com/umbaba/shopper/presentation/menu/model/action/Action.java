package com.umbaba.shopper.presentation.menu.model.action;


public interface Action {
    void perform();
}
