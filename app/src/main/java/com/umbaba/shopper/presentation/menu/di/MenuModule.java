package com.umbaba.shopper.presentation.menu.di;

import android.content.Context;

import com.umbaba.shopper.presentation.menu.model.IMenuItemsSource;
import com.umbaba.shopper.presentation.menu.model.MenuItemsSource;
import com.umbaba.shopper.presentation.menu.presenter.MenuPresenter;
import com.umbaba.shopper.presentation.menu.view.IMenuView;

import dagger.Module;
import dagger.Provides;

@Module
@MenuScope
public class MenuModule {

    private IMenuView menuView;
    private Context activityContext;

    public MenuModule(IMenuView menuView, Context activityContext) {
        this.menuView = menuView;
        this.activityContext = activityContext;
    }

    @Provides
    IMenuView menuView() {
        return menuView;
    }

    @Provides
    Context activityContext() {
        return activityContext;
    }

    @Provides
    IMenuItemsSource menuItemsSource(Context context) {
        return new MenuItemsSource(context);
    }

    @Provides
    MenuPresenter presenter(IMenuItemsSource menuItemsSource) {
        return new MenuPresenter(menuItemsSource);
    }
}
