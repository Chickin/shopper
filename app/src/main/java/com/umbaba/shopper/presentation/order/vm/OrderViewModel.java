package com.umbaba.shopper.presentation.order.vm;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.umbaba.entity.ProductTypeEntity;
import com.umbaba.shopper.order.IGetProductsByTypesUseCase;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;
import com.umbaba.shopper.presentation.order.model.mapper.IProductMapper;
import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;


public class OrderViewModel extends ViewModel {

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final IProductMapper productMapper;
    private final IGetProductsByTypesUseCase getProductsByTypesUseCase;
    MutableLiveData<List<ProductDisplayModel>> products = new MutableLiveData<>();
    MutableLiveData<List<ProductType>> productTypes = new MutableLiveData<>();

    @Inject
    public OrderViewModel(IProductMapper productMapper, IGetProductsByTypesUseCase getProductsByTypesUseCase) {
        this.productMapper = productMapper;
        this.getProductsByTypesUseCase = getProductsByTypesUseCase;
    }


    public void start(ArrayList<ProductType> productsList) {
        productTypes.postValue(productsList);
        List<ProductTypeEntity> selectedProductTypes = Observable
                .just(productsList)
                .flatMapIterable(productTypes -> productTypes)
                .map(productType -> new ProductTypeEntity(productType.getId(), productType.getName()))
                .toList().blockingGet();

        compositeDisposable.add(getProductsByTypesUseCase.getProductByTypes(selectedProductTypes)
                .flatMapIterable(productEntities -> productEntities)
                .map(productMapper::transform)
                .toList()
                .subscribe(productEntities -> products.postValue(productEntities)));
    }

    public MutableLiveData<List<ProductType>> getProductTypes() {
        return productTypes;
    }

    public MutableLiveData<List<ProductDisplayModel>> getProducts() {
        return products;
    }
}
