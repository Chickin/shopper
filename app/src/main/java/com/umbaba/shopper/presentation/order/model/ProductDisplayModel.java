package com.umbaba.shopper.presentation.order.model;


import com.umbaba.shopper.presentation.shop.model.ProductType;

public class ProductDisplayModel {
    private final String uuid;
    private final String name;
    private final int price;
    private final ProductType productType;

    public ProductDisplayModel(String uuid, String name, int price, ProductType productType) {
        this.uuid = uuid;
        this.name = name;
        this.price = price;
        this.productType = productType;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public ProductType getProductType() {
        return productType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductDisplayModel that = (ProductDisplayModel) o;

        if (price != that.price) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return productType != null ? productType.equals(that.productType) : that.productType == null;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + (productType != null ? productType.hashCode() : 0);
        return result;
    }
}
