package com.umbaba.shopper.presentation.order.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.umbaba.shopper.R;
import com.umbaba.shopper.databinding.ProductItemBinding;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;

import java.util.List;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductItemVH> {

    private List<ProductDisplayModel> productDisplayModels;

    public ProductsAdapter(List<ProductDisplayModel> productDisplayModels) {
        this.productDisplayModels = productDisplayModels;
    }

    @Override
    public ProductItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        ProductItemBinding itemBinding =
                ProductItemBinding.inflate(layoutInflater, parent, false);
        return new ProductItemVH(itemBinding);
    }

    @Override
    public void onBindViewHolder(ProductItemVH holder, int position) {
        holder.bind(productDisplayModels.get(position));
    }

    @Override
    public int getItemCount() {
        return productDisplayModels.size();
    }


    public class ProductItemVH extends RecyclerView.ViewHolder {
        private final ProductItemBinding binding;

        public ProductItemVH(ProductItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ProductDisplayModel entry) {
            binding.setProduct(entry);
            binding.productImage.setImageResource(android.R.drawable.ic_menu_send);
            binding.executePendingBindings();
        }
    }
}
