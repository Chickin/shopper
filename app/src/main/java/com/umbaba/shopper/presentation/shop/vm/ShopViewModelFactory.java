package com.umbaba.shopper.presentation.shop.vm;

import android.arch.lifecycle.ViewModelProvider;

import com.umbaba.shopper.navigation.IStartOrderUseCase;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.shop.model.mapper.CategoryMapper;
import com.umbaba.shopper.presentation.shop.presenter.ShopViewModel;
import com.umbaba.shopper.shop.IGetCategoriesUseCase;

import javax.inject.Inject;

public class ShopViewModelFactory implements ViewModelProvider.Factory {


    private final IGetCategoriesUseCase getCategoriesUseCase;
    private final CategoryMapper categoryMapper;
    private final LoadingState loading;
    private final IStartOrderUseCase startOrderUseCase;

    @Inject
    public ShopViewModelFactory(IGetCategoriesUseCase getCategoriesUseCase, IStartOrderUseCase startOrderUseCase, CategoryMapper categoryMapper, LoadingState loading) {
        this.getCategoriesUseCase = getCategoriesUseCase;
        this.startOrderUseCase = startOrderUseCase;
        this.categoryMapper = categoryMapper;
        this.loading = loading;
    }

    @Override
    public ShopViewModel create(Class modelClass) {
        return new ShopViewModel(getCategoriesUseCase, categoryMapper, startOrderUseCase, loading);
    }
}