package com.umbaba.shopper.presentation.shop.model;


import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class Category extends ExpandableGroup<ProductType> {

    private String image;

    public Category(String title, List<ProductType> items, String image) {
        super(title, items);
        this.image = image;
    }

}
