package com.umbaba.shopper.presentation.shop.model.mapper;


import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategory;

import java.util.List;

interface ICategoryMapper {
    List<MultiCheckCategory> transformList(List<CategoryEntity> categoryEntities);
}
