package com.umbaba.shopper.presentation.shop.di;

import com.umbaba.shopper.presentation.di.component.AppComponent;
import com.umbaba.shopper.presentation.shop.view.ShopSimpleActivity;

import dagger.Component;

@Component(modules = ShopModule.class, dependencies = AppComponent.class)
@ShopScreen
public interface ShopComponent {
    void inject(ShopSimpleActivity shopSimpleActivity);

}
