package com.umbaba.shopper.presentation.shop.view.expandrecycler;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.umbaba.shopper.R;
import com.umbaba.shopper.presentation.shop.model.Category;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.multicheck.MultiCheckCategory;
import com.umbaba.shopper.presentation.shop.view.expandrecycler.singlecheck.SingleCheckCategory;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class CategoryViewHolder extends GroupViewHolder {

  private TextView genreName;
  private ImageView arrow;
  private ImageView icon;

  public CategoryViewHolder(View itemView) {
    super(itemView);
    genreName = (TextView) itemView.findViewById(R.id.list_item_genre_name);
    arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);
    icon = (ImageView) itemView.findViewById(R.id.list_item_genre_icon);
  }

  public void setCategoryTitle(ExpandableGroup genre) {
    if (genre instanceof Category) {
      genreName.setText(genre.getTitle());
    //  icon.setBackgroundResource(((Category) genre).getIconResId());
    }
    if (genre instanceof MultiCheckCategory) {
      genreName.setText(genre.getTitle());
  //    icon.setBackgroundResource(((MultiCheckCategory) genre).getIconResId());
    }
    if (genre instanceof SingleCheckCategory) {
      genreName.setText(genre.getTitle());
      icon.setBackgroundResource(((SingleCheckCategory) genre).getIconResId());
    }
  }

  @Override
  public void expand() {
    animateExpand();
  }

  @Override
  public void collapse() {
    animateCollapse();
  }

  private void animateExpand() {
    RotateAnimation rotate =
        new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }

  private void animateCollapse() {
    RotateAnimation rotate =
        new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }
}
