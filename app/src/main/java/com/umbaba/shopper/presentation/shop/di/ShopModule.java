package com.umbaba.shopper.presentation.shop.di;

import android.content.Context;

import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.shopper.data.CategoryDataGateway;
import com.umbaba.shopper.data.CategoryDataStoreFactory;
import com.umbaba.shopper.data.ICategoryDataStoreFactory;
import com.umbaba.shopper.data.IDataStoreFactory;
import com.umbaba.shopper.data.repository.cache.CategoryCacheImpl;
import com.umbaba.shopper.navigation.IStartOrderUseCase;
import com.umbaba.shopper.navigation.StartOrderUseCase;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.shop.model.mapper.CategoryMapper;
import com.umbaba.shopper.presentation.shop.vm.ShopViewModelFactory;
import com.umbaba.shopper.shop.GetCategoriesUseCase;
import com.umbaba.shopper.shop.IGetCategoriesUseCase;

import dagger.Module;
import dagger.Provides;

@Module
public class ShopModule {

    private Context context;

    public ShopModule(Context context) {
        this.context = context;
    }

    @Provides
    @ShopScreen
    CategoryMapper categoryMapper() {
        return new CategoryMapper();
    }

    @Provides
    @ShopScreen
    ICategoryCache categoryCache() {
        return new CategoryCacheImpl();
    }


    @Provides
    @ShopScreen
    IDataStoreFactory<ICategoryGateway> categoryDataStoreFactory(ICategoryCache categoryCache) {
        return new CategoryDataStoreFactory(categoryCache);
    }

    @Provides
    @ShopScreen
    ICategoryGateway groceryDataRepository(IDataStoreFactory<ICategoryGateway> dataStoreFactory) {
        return new CategoryDataGateway(dataStoreFactory);
    }

    @Provides
    @ShopScreen
    IGetCategoriesUseCase categoriesUseCase(ICategoryGateway gateway) {
        return new GetCategoriesUseCase(gateway);
    }

    @Provides
    @ShopScreen
    IStartOrderUseCase startOrderUseCase() {
        return new StartOrderUseCase(context);
    }

    @Provides
    @ShopScreen
    ShopViewModelFactory shopViewModelFactory(IGetCategoriesUseCase getCategoriesUseCase, CategoryMapper mapper, LoadingState loading, IStartOrderUseCase startOrderUseCase) {
        return new ShopViewModelFactory(getCategoriesUseCase, startOrderUseCase, mapper, loading);
    }


}
