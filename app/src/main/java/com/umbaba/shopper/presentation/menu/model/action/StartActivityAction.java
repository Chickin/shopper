package com.umbaba.shopper.presentation.menu.model.action;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class StartActivityAction implements Action {


    private Context context;
    private Class<? extends Activity> activityClass;

    public StartActivityAction(Context context, Class<? extends Activity> activityClass) {
        this.context = context;
        this.activityClass = activityClass;

    }

    @Override
    public void perform() {
        context.startActivity(new Intent(context, activityClass));
    }
}
