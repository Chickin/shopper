package com.umbaba.shopper.presentation.order.view;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.umbaba.shopper.databinding.OrderGroupItemBinding;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;
import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderItemVH> {

    private Map<ProductType, List<ProductDisplayModel>> productTypes;

    public OrderAdapter(List<ProductType> productTypes) {
        this.productTypes = new LinkedHashMap<>();
        for (ProductType productType : productTypes) {
            this.productTypes.put(productType, new ArrayList<>());
        }
    }

    @Override
    public OrderItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        OrderGroupItemBinding itemBinding =
                OrderGroupItemBinding.inflate(layoutInflater, parent, false);
        return new OrderItemVH(itemBinding);
    }

    @Override
    public void onBindViewHolder(OrderItemVH holder, int position) {

        Map.Entry<ProductType, List<ProductDisplayModel>> productTypeListEntry = null;

        int i = 0;
        for (Map.Entry<ProductType, List<ProductDisplayModel>> typeListEntry : productTypes.entrySet()) {
            if (position == i) {
                productTypeListEntry = typeListEntry;
                break;
            }
            i++;
        }

        holder.bind(productTypeListEntry);
    }

    @Override
    public int getItemCount() {
        return productTypes.size();
    }

    public void assignProducts(List<ProductDisplayModel> productDisplayModels) {
        for (ProductDisplayModel productDisplayModel : productDisplayModels) {
            List<ProductDisplayModel> productDisplayModelList = productTypes.get(productDisplayModel.getProductType());
            if (productDisplayModelList == null) {
                productDisplayModelList = new ArrayList<>();
            }
            productDisplayModelList.add(productDisplayModel);
        }
    }

    public class OrderItemVH extends RecyclerView.ViewHolder {
        private final OrderGroupItemBinding binding;
        private RecyclerView.LayoutManager manager;

        public OrderItemVH(OrderGroupItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.manager = new LinearLayoutManager(binding.getRoot().getContext(), LinearLayoutManager.HORIZONTAL, false);
        }

        public void bind(Map.Entry<ProductType, List<ProductDisplayModel>> entry) {
            binding.setProductType(entry.getKey());
            List<ProductDisplayModel> value = entry.getValue();
            if (value != null) {
                binding.setProducts(value);
                binding.productsRecycler.setLayoutManager(manager);
                binding.productsRecycler.setAdapter(new ProductsAdapter(value));
            }
            binding.executePendingBindings();
        }
    }
}
