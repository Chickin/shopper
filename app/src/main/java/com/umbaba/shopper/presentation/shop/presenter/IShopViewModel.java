package com.umbaba.shopper.presentation.shop.presenter;

import android.view.View;

import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.umbaba.shopper.presentation.common.presenter.IPresenter;

import io.reactivex.Observable;


public interface IShopViewModel {
    void pickSelection();

    void subscribeOnSelection(Observable<CheckedExpandableGroup> observableSelection);
}
