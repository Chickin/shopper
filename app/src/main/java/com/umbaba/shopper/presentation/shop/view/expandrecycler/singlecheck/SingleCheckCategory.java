package com.umbaba.shopper.presentation.shop.view.expandrecycler.singlecheck;

import android.os.Parcel;

import com.thoughtbot.expandablecheckrecyclerview.models.SingleCheckExpandableGroup;

import java.util.List;

public class SingleCheckCategory extends SingleCheckExpandableGroup {

  private int iconResId;

  public SingleCheckCategory(String title, List items, int iconResId) {
    super(title, items);
    this.iconResId = iconResId;
  }

  protected SingleCheckCategory(Parcel in) {
    super(in);
    iconResId = in.readInt();
  }

  public int getIconResId() {
    return iconResId;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeInt(iconResId);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<SingleCheckCategory> CREATOR = new Creator<SingleCheckCategory>() {
    @Override
    public SingleCheckCategory createFromParcel(Parcel in) {
      return new SingleCheckCategory(in);
    }

    @Override
    public SingleCheckCategory[] newArray(int size) {
      return new SingleCheckCategory[size];
    }
  };
}
