package com.umbaba.shopper.presentation.order.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.umbaba.shopper.R;
import com.umbaba.shopper.ShopperApp;
import com.umbaba.shopper.databinding.ActivityOrderBinding;
import com.umbaba.shopper.presentation.common.view.LoadingState;
import com.umbaba.shopper.presentation.order.di.DaggerOrderComponent;
import com.umbaba.shopper.presentation.order.di.OrderComponent;
import com.umbaba.shopper.presentation.order.di.OrderModule;
import com.umbaba.shopper.presentation.order.model.ProductDisplayModel;
import com.umbaba.shopper.presentation.order.vm.OrderVMFactory;
import com.umbaba.shopper.presentation.order.vm.OrderViewModel;
import com.umbaba.shopper.presentation.shop.model.ProductType;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.umbaba.shopper.navigation.StartOrderUseCase.EXTRA_DATA;

public class OrderActivity extends AppCompatActivity {

    private static final String TAG = "OrderActivity";

    ActivityOrderBinding binding;

    @Inject
    OrderVMFactory orderModelFactory;

    @Inject
    LoadingState loadingState;
    private OrderComponent orderComponent;
    private OrderAdapter orderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        OrderViewModel orderViewModel = ViewModelProviders.of(this, orderModelFactory).get(OrderViewModel.class);
        initBinding(orderViewModel);
        orderViewModel.getProductTypes().observe(this, productTypes -> {
            binding.products.setLayoutManager(new LinearLayoutManager(this));
            orderAdapter = new OrderAdapter(productTypes);
            binding.products.setAdapter(orderAdapter);
        });
        if(savedInstanceState == null){
            ArrayList<ProductType> productsList = getIntent().getParcelableArrayListExtra(EXTRA_DATA);
            orderViewModel.start(productsList);
        }
        orderViewModel.getProducts().observe(this, productDisplayModels -> {
            orderAdapter.assignProducts(productDisplayModels);
        });

    }

    protected void initBinding(OrderViewModel orderViewModel) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order);
        binding.setVm(orderViewModel);
        binding.setLoading(loadingState);
    }

    private void inject() {
        orderComponent = DaggerOrderComponent
                .builder()
                .orderModule(new OrderModule())
                .appComponent(ShopperApp.getDaggerAppComponent())
                .build();
        orderComponent.inject(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        orderComponent = null;
    }
}
