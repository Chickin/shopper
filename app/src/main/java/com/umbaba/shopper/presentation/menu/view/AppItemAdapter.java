package com.umbaba.shopper.presentation.menu.view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.umbaba.shopper.R;
import com.umbaba.shopper.presentation.menu.model.AppMenuItem;

import java.util.List;

public class AppItemAdapter extends RecyclerView.Adapter<AppItemAdapter.AppItemViewHolder> {

    List<AppMenuItem> menuModels;
    Context context;
    private ClickListener clickListener;

    public AppItemAdapter(Context context) {
        this.context = context;
    }

    @Override
    public AppItemAdapter.AppItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app_menu, parent, false);
        return new AppItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppItemAdapter.AppItemViewHolder holder, int position) {
        holder.bind(menuModels.get(position));
    }

    @Override
    public int getItemCount() {
        return menuModels != null ? menuModels.size() : 0;
    }

    public void setAppItems(List<AppMenuItem> menus) {
        if (menus == null) {
            throw new IllegalArgumentException("List can't be null");
        }
        menuModels = menus;
        notifyDataSetChanged();
    }

    class AppItemViewHolder extends RecyclerView.ViewHolder {
        ImageView menuImage;
        TextView menuName;

        AppItemViewHolder(View itemView) {
            super(itemView);
            menuName = (TextView) itemView.findViewById(R.id.menu_item_text);
            menuImage = (ImageView) itemView.findViewById(R.id.menu_item_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.onItemClicked(v, getAdapterPosition());
                    }
                }
            });
        }

        void bind(AppMenuItem menu) {
            ((GradientDrawable) menuImage.getDrawable()).setColor(menu.getBgColor());
            menuName.setText(menu.getName());
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClicked(View v, int position);
    }

}
