package com.umbaba.shopper.data.network;


 import com.umbaba.entity.CategoryEntity;

 import java.util.List;

 import io.reactivex.Observable;
 import retrofit2.http.GET;

public interface CategoriesRemote {

    @GET("bins/15csnh")
    Observable<List<CategoryEntity>> getCategories();
}
