package com.umbaba.shopper.data.network;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.umbaba.shopper.ShopperApp;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemotesProvider {


    public static final String BASE_URL = "https://api.myjson.com/";
    static CategoriesRemote categoriesRemote;

    public static Retrofit getRetrofit(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ChuckInterceptor(ShopperApp.getContext()))
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static CategoriesRemote getCategoriesRemote(){
        if(categoriesRemote == null){
            categoriesRemote = getRetrofit().create(CategoriesRemote.class);
        }
        return categoriesRemote;
    }





}
