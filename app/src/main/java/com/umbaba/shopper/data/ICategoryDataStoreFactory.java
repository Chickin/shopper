package com.umbaba.shopper.data;


import com.umbaba.shopper.boundries.ICategoryGateway;

public interface ICategoryDataStoreFactory {
    ICategoryGateway create();
}
