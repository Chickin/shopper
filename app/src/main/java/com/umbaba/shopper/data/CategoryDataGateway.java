package com.umbaba.shopper.data;


import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.entity.CategoryEntity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;


public class CategoryDataGateway implements ICategoryGateway {
    private final IDataStoreFactory<ICategoryGateway> categoryDataStoreFactory;

    public CategoryDataGateway(IDataStoreFactory<ICategoryGateway> categoryDataStoreFactory) {
        this.categoryDataStoreFactory = categoryDataStoreFactory;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        return categoryDataStoreFactory.create().categories();
    }
}
