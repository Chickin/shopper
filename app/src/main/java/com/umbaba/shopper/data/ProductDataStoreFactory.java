package com.umbaba.shopper.data;


import com.umbaba.cache.IProductsCache;
import com.umbaba.shopper.boundries.IProductsGateway;
import com.umbaba.shopper.data.repository.datasource.ProductCloudDataStore;
import com.umbaba.shopper.data.repository.datasource.ProductLocalDataStore;

public class ProductDataStoreFactory implements IDataStoreFactory<IProductsGateway> {

    private IProductsCache productsCache;

    public ProductDataStoreFactory(IProductsCache productsCache) {
        this.productsCache = productsCache;
    }

    @Override
    public IProductsGateway create() {
        if (productsCache.isCached() && !productsCache.isExpired()) {
            return new ProductLocalDataStore(productsCache);
        } else {
            return new ProductCloudDataStore(productsCache);
        }
    }
}
