package com.umbaba.shopper.data;


public interface IDataStoreFactory<T> {
    T create();
}
