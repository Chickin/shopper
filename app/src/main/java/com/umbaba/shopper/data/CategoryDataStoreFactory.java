package com.umbaba.shopper.data;


import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.shopper.data.repository.datasource.CategoryCloudDataStore;
import com.umbaba.shopper.data.repository.datasource.CategoryLocalDataStore;

public class CategoryDataStoreFactory implements IDataStoreFactory<ICategoryGateway> {

    private ICategoryCache categoryCache;

    public CategoryDataStoreFactory( ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    @Override
    public ICategoryGateway create() {
        if (categoryCache.isCached() && !categoryCache.isExpired()) {
            return new CategoryLocalDataStore(categoryCache);
        } else {
            return new CategoryCloudDataStore(categoryCache);
        }
    }
}
