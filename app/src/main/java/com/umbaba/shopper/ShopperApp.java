package com.umbaba.shopper;


import android.app.Application;

import com.umbaba.shopper.presentation.di.component.AppComponent;
import com.umbaba.shopper.presentation.di.component.DaggerAppComponent;
import com.umbaba.shopper.presentation.di.module.AppModule;

public class ShopperApp extends Application {

    static AppComponent daggerAppComponent;
    private static ShopperApp context;

    public static ShopperApp getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        daggerAppComponent = DaggerAppComponent.builder().appModule(new AppModule(context)).build();
    }

    public static AppComponent getDaggerAppComponent() {
        return daggerAppComponent;
    }

}
