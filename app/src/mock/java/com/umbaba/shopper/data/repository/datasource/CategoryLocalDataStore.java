package com.umbaba.shopper.data.repository.datasource;


import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;

import java.util.List;

import io.reactivex.Observable;

public class CategoryLocalDataStore implements ICategoryGateway {
    private ICategoryCache categoryCache;

    public CategoryLocalDataStore(ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        return categoryCache.get();
    }
}
