package com.umbaba.shopper.data.repository.datasource;

import com.umbaba.cache.IProductsCache;
import com.umbaba.entity.ProductEntity;
import com.umbaba.shopper.boundries.IProductsGateway;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


public class ProductCloudDataStore implements IProductsGateway {
    public static final String PRODUCT_UUID_0 = "ca77e48d-0-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_1 = "ca77e48d-1-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_2 = "ca77e48d-2-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_3 = "ca77e48d-3-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_4 = "ca77e48d-4-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_5 = "ca77e48d-5-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_6 = "ca77e48d-6-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_7 = "ca77e48d-7-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_8 = "ca77e48d-8-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_9 = "ca77e48d-9-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_10 = "ca77e48d-10-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_11 = "ca77e48d-11-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_12 = "ca77e48d-12-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_13 = "ca77e48d-13-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_14 = "ca77e48d-14-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_15 = "ca77e48d-15-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_16 = "ca77e48d-16-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_17 = "ca77e48d-17-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_18 = "ca77e48d-18-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_19 = "ca77e48d-19-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_UUID_20 = "ca77e48d-20-4c8f-b754-9bbbe2704266";


    public static final String PRODUCT_NAME_0 = "name0";
    public static final String PRODUCT_NAME_1 = "name1";
    public static final String PRODUCT_NAME_2 = "name2";
    public static final String PRODUCT_NAME_3 = "name3";
    public static final String PRODUCT_NAME_4 = "name4";
    public static final String PRODUCT_NAME_5 = "name5";
    public static final String PRODUCT_NAME_6 = "name6";
    public static final String PRODUCT_NAME_7 = "name7";
    public static final String PRODUCT_NAME_8 = "name8";
    public static final String PRODUCT_NAME_9 = "name9";
    public static final String PRODUCT_NAME_10 = "name10";
    public static final String PRODUCT_NAME_11 = "name11";
    public static final String PRODUCT_NAME_12 = "name12";
    public static final String PRODUCT_NAME_13 = "name13";
    public static final String PRODUCT_NAME_14 = "name14";
    public static final String PRODUCT_NAME_15 = "name15";
    public static final String PRODUCT_NAME_16 = "name16";
    public static final String PRODUCT_NAME_17 = "name17";
    public static final String PRODUCT_NAME_18 = "name18";
    public static final String PRODUCT_NAME_19 = "name19";
    public static final String PRODUCT_NAME_20 = "name20";

    public static final int PRODUCT_PRICE_0 = 5;
    public static final int PRODUCT_PRICE_1 = 10;
    public static final int PRODUCT_PRICE_2 = 20;
    public static final int PRODUCT_PRICE_3 = 30;
    public static final int PRODUCT_PRICE_4 = 40;


    public static final String PRODUCT_TYPE_UUID_0 = "zx45a57f-1-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_1 = "zx45a57f-2-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_2 = "zx45a57f-3-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_3 = "zx45a57f-4-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_4 = "zx45a57f-5-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_5 = "zx45a57f-6-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_6 = "zx45a57f-7-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_7 = "zx45a57f-8-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_8 = "zx45a57f-9-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_9 = "zx45a57f-10-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_10 = "zx45a57f-11-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_11 = "zx45a57f-12-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_12 = "zx45a57f-13-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_13 = "zx45a57f-14-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_14 = "zx45a57f-15-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_15 = "zx45a57f-16-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_16 = "zx45a57f-17-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_17 = "zx45a57f-18-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_18 = "zx45a57f-19-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_19 = "zx45a57f-20-4c8f-b754-9bbbe2704266";
    public static final String PRODUCT_TYPE_UUID_20 = "zx45a57f-21-4c8f-b754-9bbbe2704266";
    private final IProductsCache productsCache;

    public ProductCloudDataStore(IProductsCache productsCache) {
        this.productsCache = productsCache;
    }


    @Override
    public Observable<List<ProductEntity>> products() {

        List<ProductEntity> PRODUCT_ENTITIES = new ArrayList<>();

        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_0, PRODUCT_NAME_0, PRODUCT_TYPE_UUID_0, PRODUCT_PRICE_0));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_1, PRODUCT_NAME_1, PRODUCT_TYPE_UUID_1, PRODUCT_PRICE_1));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_2, PRODUCT_NAME_2, PRODUCT_TYPE_UUID_2, PRODUCT_PRICE_2));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_3, PRODUCT_NAME_3, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_4, PRODUCT_NAME_4, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_5, PRODUCT_NAME_5, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_6, PRODUCT_NAME_6, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_7, PRODUCT_NAME_7, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_8, PRODUCT_NAME_8, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_9, PRODUCT_NAME_9, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_10, PRODUCT_NAME_10, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_11, PRODUCT_NAME_11, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_12, PRODUCT_NAME_12, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_13, PRODUCT_NAME_13, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));
        PRODUCT_ENTITIES.add(new ProductEntity(PRODUCT_UUID_14, PRODUCT_NAME_14, PRODUCT_TYPE_UUID_3, PRODUCT_PRICE_3));

        return Observable.just(PRODUCT_ENTITIES);
    }
}
