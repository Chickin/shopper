package com.umbaba.shopper.data.repository.datasource;


import com.umbaba.shopper.boundries.ICategoryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;
import com.umbaba.entity.ProductTypeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

import static com.umbaba.shopper.data.repository.datasource.ProductCloudDataStore.*;

public class CategoryCloudDataStore implements ICategoryGateway {
    private ICategoryCache categoryCache;

    public CategoryCloudDataStore(ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        final List<CategoryEntity> categoryEntities = new ArrayList<>();
        CategoryEntity fruits = new CategoryEntity("Fruits", "asd");
        fruits.setLastUpdated(System.currentTimeMillis());
        ArrayList<ProductTypeEntity> fruitProducts = new ArrayList<>();
        fruitProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_0, "Banana"));
        fruitProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_1, "Apple"));
        fruitProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_2, "Pineapple"));
        fruitProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_3, "Lemon"));
        fruits.setProducts(fruitProducts);
        categoryEntities.add(fruits);


        ArrayList<ProductTypeEntity> bakeryProducts = new ArrayList<>();

        CategoryEntity bakery = new CategoryEntity("Bakery", "asd");
        bakery.setLastUpdated(System.currentTimeMillis());

        bakeryProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_4, "Bread"));
        bakeryProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_5, "Bun"));
        bakeryProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_6, "Baguette"));
        bakery.setProducts(bakeryProducts);
        categoryEntities.add(bakery);


        ArrayList<ProductTypeEntity> dairyProducts = new ArrayList<>();

        CategoryEntity dairy = new CategoryEntity("Dairy", "asd");
        dairy.setLastUpdated(System.currentTimeMillis());
        dairyProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_7, "Milk"));
        dairyProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_8, "Yogurt"));
        dairyProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_9, "Kefir"));
        dairyProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_10, "Sour cream"));
        dairyProducts.add(new ProductTypeEntity(PRODUCT_TYPE_UUID_11, "Cottage cheese"));
        dairy.setProducts(dairyProducts);
        categoryEntities.add(dairy);
        return Observable.fromArray(categoryEntities).doOnNext(list -> categoryCache.put(list)).delay(5, TimeUnit.SECONDS);
    }
}
