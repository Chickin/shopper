package com.umbaba.shopper.data.repository;


import com.umbaba.IGroceryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.shopper.data.repository.datasource.CategoryCloudDataStore;
import com.umbaba.shopper.data.repository.datasource.CategoryLocalDataStore;

public class CategoryDataStoreFactory {
    private ICategoryCache categoryCache;

    public CategoryDataStoreFactory(ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    public IGroceryGateway create() {
        if(!categoryCache.isExpired() && categoryCache.isCached()){
            return new CategoryLocalDataStore(categoryCache);
        }else{
            return new CategoryCloudDataStore(categoryCache);
        }
    }
}
