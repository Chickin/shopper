package com.umbaba.shopper.data.repository.cache;

import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class CategoryCacheImpl implements ICategoryCache {


    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public boolean isCached() {
        return false;
    }

    @Override
    public Observable<List<CategoryEntity>> get() {
        return null;
    }

    @Override
    public void put(List<CategoryEntity> townshipEntities) {

    }
}
