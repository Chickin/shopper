package com.umbaba.shopper.data.repository.datasource;


import com.umbaba.IGroceryGateway;
import com.umbaba.cache.ICategoryCache;
import com.umbaba.entity.CategoryEntity;
import com.umbaba.shopper.data.network.RemotesProvider;

import java.util.List;

import io.reactivex.Observable;

public class CategoryCloudDataStore  implements IGroceryGateway {
    private ICategoryCache categoryCache;

    public CategoryCloudDataStore(ICategoryCache categoryCache) {
        this.categoryCache = categoryCache;
    }

    @Override
    public Observable<List<CategoryEntity>> categories() {
        return RemotesProvider.getCategoriesRemote().getCategories().doOnNext(categoryEntities -> categoryCache.put(categoryEntities));
    }
}
